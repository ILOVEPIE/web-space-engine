local:
	sh ./sbin/commands/local-compile.sh
remote:
	sh ./sbin/commands/remote-compile.sh
clean:
	sh ./sbin/commands/clean.sh
cleanup:
	sh ./sbin/commands/cleanup.sh
install-testenv:
	sh ./sbin/commands/install-test-environments.sh
buildfix:
	find ./sbin/ -name *.sh -print | xargs chmod +x