#!/bin/sh
. "$PWD/sbin/bootstrap.sh"
"$TOOL_BIN_DIR/jsvu"
rm -rf "$TOOL_BIN_DIR/engines"
mv -f ~/.jsvu/* "$TOOL_BIN_DIR"
rm -rf ~/.jsvu/
rm "$TOOL_DATA_DIR/eshost-config.json"
if [ -f "$TOOL_BIN_DIR/engines/chakra/chakra" ]; then
    rm -f "$TOOL_BIN_DIR/ch"
    rm -f "$TOOL_BIN_DIR/chakra"
    $TOOL_BIN_DIR/eshost --config "$TOOL_DATA_DIR/eshost-config.json" --add 'Chakra' ch "$TOOL_BIN_DIR/engines/chakra/chakra"
fi
if [ -f "$TOOL_BIN_DIR/engines/javascriptcore/javascriptcore" ]; then
    rm -f "$TOOL_BIN_DIR/jsc"
    rm -f "$TOOL_BIN_DIR/javascriptcore"
    $TOOL_BIN_DIR/eshost --config "$TOOL_DATA_DIR/eshost-config.json" --add 'JavaScriptCore' jsc "$TOOL_BIN_DIR/javascriptcore"
fi
if [ -f "$TOOL_BIN_DIR/engines/spidermonkey/spidermonkey" ]; then
    rm -f "$TOOL_BIN_DIR/sm"
    rm -f "$TOOL_BIN_DIR/spidermonkey"
    echo "#!/bin/sh" > "$TOOL_BIN_DIR/spidermonkey"
    echo "LD_LIBRARY_PATH=\"$TOOL_BIN_DIR/engines/spidermonkey\" \"$TOOL_BIN_DIR/engines/spidermonkey/spidermonkey\" \"\$@\"" >> "$TOOL_BIN_DIR/spidermonkey"
    chmod +x "$TOOL_BIN_DIR/spidermonkey"
    $TOOL_BIN_DIR/eshost --config "$TOOL_DATA_DIR/eshost-config.json" --add 'SpiderMonkey' jsshell "$TOOL_BIN_DIR/spidermonkey"
fi
if [ -f "$TOOL_BIN_DIR/engines/v8/v8" ]; then
    rm -f "$TOOL_BIN_DIR/v8"
    echo "#!/bin/sh" > "$TOOL_BIN_DIR/v8"
    echo "\"$TOOL_BIN_DIR/engines/v8/v8\" --natives_blob=\"$TOOL_BIN_DIR/engines/v8/natives_blob.bin\" --snapshot_blob=\"$TOOL_BIN_DIR/engines/v8/snapshot_blob.bin\" \"\$@\"" >> "$TOOL_BIN_DIR/v8"
    chmod +x "$TOOL_BIN_DIR/v8"
    $TOOL_BIN_DIR/eshost --config "$TOOL_DATA_DIR/eshost-config.json" --add 'V8 --harmony' d8 "$TOOL_BIN_DIR/v8" --args '--harmony'
    $TOOL_BIN_DIR/eshost --config "$TOOL_DATA_DIR/eshost-config.json" --add 'V8' d8 "$TOOL_BIN_DIR/v8"
fi