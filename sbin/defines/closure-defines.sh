#!/bin/sh
export CLOSURE_LATEST="http://dl.google.com/closure-compiler/compiler-latest.tar.gz"
export CLOSURE_REMOTE_API="https://closure-compiler.appspot.com/compile"


export CLOSURE_NAME="closure.jar"

export CLOSURE_LOGGING_DETAIL="VERBOSE"

export CLOSURE_COMPILATION_LEVEL="ADVANCED_OPTIMIZATIONS"

export CLOSURE_INPUT_LANGUAGE_VERSION="ECMASCRIPT6_STRICT"
export CLOSURE_OUTPUT_LANGUAGE_VERSION="ECMASCRIPT5"

export CLOSURE_ENABLE_TYPED_OPTIMIZATION="true"

export CLOSURE_TYPE_INF_OPTION_OLD=
export CLOSURE_TYPE_INF_OPTION_NEW=--new_type_inf

export CLOSURE_TYPE_INF=$CLOSURE_TYPE_INF_OPTION_NEW

export CLOSURE_OUTPUT_WRAPPER_PREFIX="\"(function(global,external){var webspace=external.\$;%output%if(++webspace.\$==="
export CLOSURE_OUTPUT_WRAPPER_SUFFIX="){external.\$=null;webspace.\$=null;webspace.init()}})(self,self.webspace=(self.webspace||{\$:{\$:0}}));\""